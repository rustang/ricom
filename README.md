ricom
=====

> A Rust DICOM Toolkit

Reference Documentation:
------------------------
Since we are building a DICOM toolkit, it is very important for us to understand the DICOM spec. Our first goal is to write a DICOM parser, the documentation is at http://dicom.nema.org/dicom/2004/04_05pu.pdf


Reference Implementation:
-------------------------
This implementation refers heavily on the pixel med's DICOM Toolkit http://www.pixelmed.com/dicomtoolkit.html
This note here is not to endorse ricom, it is only to give credit to the implementation we are referring to

Need Help
---------

This toolkit is still under development(To say this toolkit is still under development would be an exaggeration).
I would need all the help I can get to build this toolkit.
What help would I need?

 - Need motivated individuals versed in DICOM to help us understand the domain to write a good toolkit
 - Since this toolkit would be written in rust we would need someone who could code in rust


