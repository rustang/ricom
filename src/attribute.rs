
mod

pub struct Attribute {
    tag: AttributeTag,
    value_length: usize,
    value_multiplicity: usize,
}

impl Attribute {
    fn new(&self, t: AttributeTag) -> Attribute {
        Attribute {
            tag: t,
            value_length: 0,
            value_multiplicity: 0,
        }
    }

    fn get_tag(self) -> AttributeTag {
        self.tag
    }

    fn get_vl(&self) -> usize {
        self.value_length
    }

    fn get_vm(&self) -> usize {
        self.value_multiplicity
    }
}
