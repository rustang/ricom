

pub struct AttributeTag {
    pub group: i16,
    pub element: i16,
}

impl AttributeTag {
    fn get_group(&self) -> i16 {
        self.group
    }

    fn get_element(&self) -> i16 {
        self.element
    }
}