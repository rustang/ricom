mod tag_from_name;
mod attribute_tag;

use tag_from_name::TagName;
use attribute_tag::AttributeTag;

fn main() {
    println!("Hello, world!");
    TagName::AffectedSOPClassUID(AttributeTag {
        group: 0x0000,
        element: 0x0000,
    });
}
